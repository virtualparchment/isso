# First, compile JS stuff
FROM node:erbium-buster AS base
RUN git clone --depth=1 --shallow-submodules https://github.com/posativ/isso.git /src/
WORKDIR /src/
COPY . .
RUN npm install -g requirejs uglify-js jade bower \
 && make init js

# Second, create virtualenv
FROM python:3.8-buster AS python
WORKDIR /src/
COPY --from=base /src .
RUN python3 -m venv /isso \
 && . /isso/bin/activate \
 && pip3 install --no-cache-dir --upgrade pip \
 && pip3 install --no-cache-dir gunicorn cffi flask \
 && python setup.py install \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Third, create final repository
FROM python:3.8-slim-buster
WORKDIR /isso/
COPY --from=python /isso .

# Configuration
VOLUME /db /config
EXPOSE 8080
ENV ISSO_SETTINGS /config/isso.cfg
CMD ["/isso/bin/gunicorn", "-b", "0.0.0.0:8080", "-w", "1", "--preload", "isso.run", "--worker-tmp-dir", "/dev/shm"]